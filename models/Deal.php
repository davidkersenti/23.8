<?php

namespace app\models;

use Yii;
use app\models\Lead;

/**
 * This is the model class for table "deal".
 *
 * @property integer $id
 * @property integer $leadid
 * @property string $name
 * @property integer $amount
 *
 * @property Lead $lead
 */
class Deal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'leadid', 'name', 'amount'], 'required'],
            [['id', 'leadid', 'amount'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['leadid'], 'exist', 'skipOnError' => true, 'targetClass' => Lead::className(), 'targetAttribute' => ['leadid' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leadid' => 'Leadid',
            'name' => 'Name',
            'amount' => 'Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLead()
    {
        return $this->hasOne(Lead::className(), ['id' => 'leadid']);
    }
	///////////////////
	public static function getDeals()  ////////////
	{
		$allDeals = self::find()->all();
		$allDealsArray = ArrayHelper::
					map($allDeals, 'id', 'name');
		return $allDealsArray;
	}
	////////////////////
	
	/////////
	public function getDealItem() /////////////////////////
    {
        return $this->hasOne(Lead::className(), ['id' => 'leadId']);
		
    ///////////

}
}